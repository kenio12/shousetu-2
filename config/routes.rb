Rails.application.routes.draw do

  get "/rooms/:user_id", :to=>"rooms#show",param: :username,as: :room

  resources :ikebanas
  resources :shousetukansous
  devise_for :admin_users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'


  resources :comments

  resources :news

  resource :votes

  root 'home#index'



  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    :registrations => 'users/registrations' }


   resources :users do
     resources :user_abouts ,:only => [:index,:create,:update,:show]
   end

  resources :novels ,:as => :novels

  get '/novels/short_short_story', to: 'novels#short_short_story', as: :short_short_story

  get '/ssses/shousetukansous', to: 'ssses#shousetukansous', as: :ssses_shousetukansous

  get '/ssses/user_about', to: 'ssses#user_about', as: :ssses_user_about

  post '/ssses/user_about', to: 'ssses#create_user_about'
  patch '/ssses/user_about', to: 'ssses#update_user_about'
  put '/ssses/user_about', to: 'ssses#update_user_about'

  # ここはsssesのコメント部

  post '/ssses/comment', to: 'ssses#comment_create'


# コメントを編集したいが、黒い画面に飛ぶ　まだ未実装
  patch '/ssses/comment', to: 'ssses#comment_update'
  put '/ssses/comment', to: 'ssses#comment_update'

  # ここはsssesのお知らせedit お試し中
  post '/ssses/news', to: 'ssses#create_news'
  patch '/ssses/news', to: 'ssses#update_news'
  put '/ssses/news', to: 'ssses#update_news'

  get '/ssses/ichiran', to: 'ssses#ichiran' ,as: :ssses_ichiran

  get '/ssses/chokkin_novels_zenran', to: 'ssses#chokkin_novels_zenran' ,as: :ssses_chokkin_novels_zenran

  # うるまさん居ない時に　4/12

  get '/ssses/show_user_about_novel', to: 'ssses#show_user_about_novel',as: :ssses_user_about_novel

  # get '/ssses/shousetukansous/:shousetukansou_id' => 'ssses#show_shousetukansou',path: "ssses/shousetukansous/:shousetukansou_id" ,as: :ssses_shousetukansou
  # get '/ssses/shousetukansous/new' => 'ssses#new_shousetukansou',path: "ssses/shousetukansous/new" ,as: :new_ssses_shousetukansou
  # get '/ssses/shousetukansous/:shousetukansou_id/edit' => 'ssses#edit_shousetukansou',path: "ssses/shousetukansous/:shousetukansou_id/edit" ,as: :edit_ssses_shousetukansou
  # post '/ssses/shousetukansous' => 'ssses#create_shousetukansou',path: "ssses/shousetukansous/" ,as: :ssses_shousetukansous
  # patch '/ssses/shousetukansous/:shousetukansou_id' => 'ssses#update_shousetukansou',path: "ssses/shousetukansou/" ,as: :ssses_shousetukansou
  # put '/ssses/shousetukansous/:shousetukansou_id' => 'ssses#update_shousetukansou',path: "ssses/shousetukansou/" ,as: :ssses_shousetukansou


# うるまさんがいない時に9/10

  get '/ssses/message_ichiran', to: 'ssses#message_ichiran', as: :ssses_message_ichiran

  resources :ssses ,:as => :ssses


  resources :shougis

  get 'about', to: 'home#about', as: :about
  get 'tukaikata', to: 'home#tukaikata',as: :tukaikata

  mount ActionCable.server => '/cable'

  get '/ssses/vote/result', to: 'votes#result' ,as: :ssses_votes_result

end
