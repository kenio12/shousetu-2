json.extract! shousetukansou, :id, :image, :shuppansha, :chosha, :shoseki, :comment, :hyouka, :user_id, :created_at, :updated_at
json.url shousetukansou_url(shousetukansou, format: :json)