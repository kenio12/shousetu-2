json.extract! ikebana, :id, :created_at, :updated_at
json.url ikebana_url(ikebana, format: :json)