class RoomChannel < ApplicationCable::Channel
  # room.coffeeに移動
  def subscribed
    stream_from "room_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    # ActionCable.server.broadcast 'room_channel', message: data['message']
    # 入力内容、送り主ID、送り先ID を保存。
    # Message.create(content:”hoge”, sender_id: 3, receiver_id: 5)
    # Message.create{
    # content:”hoge”,
    # sender_id: 3,
    # receiver_id: 5
    # }
    #  特殊に見えるが、上のcreateと一緒。
    Message.create! content: data['message'],sender_id: data['sender_id'],receiver_id: data['receiver_id']
    # gon.sender_id = data['sender_id']
    # gon.receiver_id = data['receiver_id']
  end
end
