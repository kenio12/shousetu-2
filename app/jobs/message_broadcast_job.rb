class MessageBroadcastJob < ApplicationJob
  queue_as :default
# ここに訪れたら、performが呼び出されるものだ。
  def perform(message)
    logger.debug"---------#{message.sender_id}----------"
    # ActionCable.server.broadcast はこのページを開いている人にメッセージ内容を配信する。次に'room_channel'により、room_channelに移動。
    # なお、下のrender定義して。_message.html.erbの中身とsender_idを持って移動しているよ。
    ActionCable.server.broadcast 'room_channel', message: render_message(message),sender_id: message.sender_id
  end

  private
  # ここはrender　しているだけ。ここはjobなのでrenderが使えない。ここでは、_message.html.erbの中身を呼び出して、中身を取ってきね。
    def render_message(message)
      ApplicationController.renderer.render(partial: 'messages/message', locals: { message: message })
    end
end
