# ここはフロントサイドのファイルであり、サーバーサイドは"RoomChannel"だと指定している。
App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

# ブルードキャストから（room_channel経由）移動してきて、ココ。なお、dataは。_message.html.erbの中身とsender_id。
  received: (data) ->
    console.log(data)
    sender_id = data["sender_id"]
    # gon はgem。　（coffee）ジャバスクリプトでもcurrent_user_idが使えるようにしているもの。なお、使うためにはコントローラーで定義しておく必要あり。
    if gon.sender_id == sender_id
      # $('#send_messages').append data['message']
      $('#messages').append "<div class='kaiwa'>
       <figure class='kaiwa-img-right'>

         <img src='#{gon.my_image_url}' alt='no-img2'>

       <figcaption class='kaiwa-img-description'>

       </figcaption>
       </figure>
       <div class='kaiwa-text-left'>
         <p class='kaiwa-text'></p>
       </div>
      </div>"
      message=data["message"].replace( /\r?\n/g, '<br />' )
      $("#messages").find(".kaiwa-text").last().html("<p class='kaiwa-text'>
           #{message}
           </p>")
    else

      # $('#recieve_messages').append data['message']
      $('#messages').append "
      <div class='kaiwa'>
       <figure class='kaiwa-img-left'>

         <img src='#{gon.you_image_url}' alt='no-img2'>

       <figcaption class='kaiwa-img-description'>

       </figcaption>
       </figure>
       <div class='kaiwa-text-right'>
         <p class='kaiwa-text'></p>
       </div>
      </div>"
      message=data["message"].replace( /\r?\n/g, '<br />' )
      #　クラスの会話テキストを探して、その最後の部分を、置き換える。もう一度Pタグで置き換えしている。
      $("#messages").find(".kaiwa-text").last().html("<p class='kaiwa-text'>
          #{message}
           </p>")

    # Called when there's incoming data on the websocket for this channel
# ここで、@performを使うことで、サーバーサイトである"RoomChannel"の'speak'を呼び出し、パラメーターを渡している。配列の入力内容、送り主ID、送り先IDを各々に入れた。 。
  speak: (message) ->
    @perform 'speak', message: message[0],sender_id: message[1],receiver_id:message[2]

# room_speakerが付いているインプットタグであり、かつ、キー押された時に動作　　　　eventはデータの総称　$(document).on ‘keypress’{ここに処理を書く}
#  -> は　{ } みたいなもの。それをシンプルに記述するcoofee sarpt ならでは。終わりはインデントを空けることでわかる。
# eventは引数みたいなもの。何でもよい。event がないと　エンターキーの１３が取れない
# $(document).on 'keypress', '[data-behavior~=room_speaker]', (event) ->
# # １３のエンターキーが押された時だけ
#   if event.keyCode is 13 # return = send
#     # this は入力部分で、その親すなわちDIVタグの中にnameがsender_idのinputタグの値を取ってきてね
#     sender_id = $(this).parent().find("input[name='sender_id']").val()
# #  上と同じ
#     receiver_id = $(this).parent().find("input[name='receiver_id']").val()
#     # event.target.value　入力内容、送り主ID、送り先ID を上のApp.room.speakに配列で渡している。
#     App.room.speak [event.target.value, sender_id,receiver_id]
#     # 入力フォームの値を消す。
#     event.target.value = ''
#     # 動作が完了するまで待ってね。またないと、トランザクションになる。
#     event.preventDefault()

# くりっくしたら　メッセージスブミットのID　event はデータを取ってこれる。例えば座標とか。

window.onload = ->
  $("html,body").animate({scrollTop:$('#message-submit').offset().top});


$(document).on 'click', '#message-submit', (event) ->

#  buttonの親である<div>の中のテキストエリアの値
  value=$(this).parents("form").find("textarea").val()
  console.log(event)
# = not not value　＝　valueが空っぽではなくnillでもない時に
  if valueNotEmpty = not not value # return = send
    # this は入力部分で、その親すなわちDIVタグの中にnameがsender_idのinputタグの値を取ってきてね
    sender_id = $(this).parents("form").find("input[name='sender_id']").val()
#  上と同じ
    receiver_id = $(this).parents("form").find("input[name='receiver_id']").val()
    # event.target.value　入力内容、送り主ID、送り先ID を上のApp.room.speakに配列で渡している。

    App.room.speak [value, sender_id,receiver_id]
    # 送信ぼたんの親のdiv内からテキストエリアを探してきて、値を空にする。
    $(this).parents("form").find("textarea").val("")
    # スクロールさせる
    $("html,body").animate({scrollTop:$('#message-submit').offset().top});
    # 動作が完了するまで待ってね。またないと、トランザクションになる。
    event.preventDefault()
