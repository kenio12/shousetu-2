$(function(){

  var fileName;

  // 画像ファイル選択後のプレビュー処理
  $('form').on('change', 'input[type="file"]', function(event) {
    var file = event.target.files[0];
    fileName = file.name;
    var reader = new FileReader();
    var $crop_area_box = $('#crop_area_box');
    // 画像ファイル以外の場合は何もしない
    if(file.type.indexOf('image') < 0){
      return false;
    }
    // ファイル読み込みが完了した際のイベント登録
    reader.onload = (function(file) {
      return function(event) {
        //既存のプレビューを削除
        $crop_area_box.empty();
        // .prevewの領域の中にロードした画像を表示するimageタグを追加
        $crop_area_box.append($('<img>').attr({
          src: event.target.result,
          id: "crop_image",
          title: file.name
        }));
        // プレビュー処理に対して、クロップ出来る処理を初期化設定
        initCrop();
      };
    })(file);
    reader.readAsDataURL(file);
  });

  var cropper;
  function initCrop() {
    cropper = new Cropper(crop_image, {
      dragMode: 'move', // 画像を動かす設定
      aspectRatio: 1 / 1, // 正方形やで！
      restore: false,
      guides: false,
      center: false,
      highlight: false,
      cropBoxMovable: false,
      cropBoxResizable: false,
      toggleDragModeOnDblclick: false,
      minCropBoxWidth: 600,
      minCropBoxHeight: 600,
      maxCropBoxWidth: 600,
      maxCropBoxHeight: 600,
      ready: function () {
        croppable = true;
      }
    });
    // 初回表示時
    crop_image.addEventListener('ready', function(e){
      cropping(e);
    });
    // 画像をドラッグした際の処理
    crop_image.addEventListener('cropend', function(e){
      cropping(e);
    });
    // 画像を拡大・縮小した際の処理
    crop_image.addEventListener('zoom', function(e){
      cropping(e);
    });
  }

  // クロップ処理した画像をプレビュー領域に表示
  var croppedCanvas;
  function cropping(e) {
    croppedCanvas = cropper.getCroppedCanvas({
      width: 1200,
      height:1200,
    });
    // `$('<img>'{src: croppedCanvas.toDataURL()});` 的に書きたかったけど、jQuery力が足りず・・・
    var croppedImage = document.createElement('img');
    croppedImage.src = croppedCanvas.toDataURL();
    crop_preview.innerHTML = '';
    crop_preview.appendChild(croppedImage);
  }

  function submitAjax(formData) {
    const ikebana_id = $('#id').val();

    if(ikebana_id.length){
      $.ajax('/ikebanas/'+ikebana_id, {
        method: "PUT", // POSTの方が良いのかな？
        data: formData,
        dataType: 'script',
        processData: false, // 余計な事はせず、そのままSUBMITする設定？
        contentType: false,
      });
   } else {
      $.ajax('/ikebanas', {
        method: "POST", // POSTの方が良いのかな？
        data: formData,
        dataType: 'script',
        processData: false, // 余計な事はせず、そのままSUBMITする設定？
        contentType: false,
  })}
  }

  // Submit時に実行するPOST処理
  $('#submit').on('click', function(event){

    var formData = new FormData();
    formData.append('ikebana[title]', $("#ikebanas_title").val());
    formData.append('ikebana[message]', $("#ikebanas_message").val());

    flowers = []

    $(".flower_name").each(function(){
      if ($(this).val() != '' && flowers.indexOf($(this).val()) == -1){
      flowers.push($(this).val())
      }
    });
    formData.append('ikebana[flowers]',flowers);

    // クロップ後のファイルをblobに変換し、AjaxでForm送信
    if(croppedCanvas != undefined){
      croppedCanvas.toBlob(function (blob) {
        console.log(blob)
        const fileOfBlob = new File([blob], fileName);
        console.log(fileOfBlob)
        formData.append('ikebana[image]', fileOfBlob);
        submitAjax(formData)
      }, 'image/jpeg', 1);
    } else{
        submitAjax(formData)    
    }
  });
  var count = $('.flower_name').length-1;

  $(document).on('click','#add-select',function(){
    count++
    console.log(count)
    var select_text = '<select name="ikebanas[flower]['+count+']" id="ikebanas_flower_'+count+'" class = "flower_name" ><option value="" label=" "></option>'
    var contents = gon.flower_contents
    for(var i = 0; i < contents.length; i++) {
      select_text += '<option value="'+contents[i][0]+'">'+contents[i][1]+'</option>'
    }
    select_text += '</select>'

    $('#flower_select_area').append(select_text);
  });

});
