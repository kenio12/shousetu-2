// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery-ui
//= require tag-it
//= require nested_form_fields
//= require bootstrap-sprockets
//= require jquery.infinitescroll
//= require_tree .



$(function(){
	$('.count').bind('keydown keyup change',function(){
		var tnum  = $(this).val().replace(/[\n\s　]/g, "").length;
		$('#txtnum').text(tnum);
		$('#txtnum2').text(tnum);
	});
});

// 行を選択するとtrの中になるhrefに遷移する
$(function(){
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
	$('tbody tr[data-href]').addClass('clickable')
    //クリックイベント
    .click(function(e) {
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
        //その要素の先祖要素で一番近いtrの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('tr').data('href');
      };
  });

$(function(){
	// $('#read_more').click(function(){
	// 	 class_name = $('#read_more').attr('class');
	// 	 console.log(class_name);
	// 	 class_name = '.' + class_name;
	//    $(class_name).css("display","none");
	// });

	$(document).on('click', '.read_more', function(){
		id_name = $(this).attr('id');
		console.log(id_name);
		id_name = '#' + id_name;
		$(id_name).css("display","none");
  });

	// $("#aisatu_mugen .page").infinitescroll({
	//     loading: {
	//       img:     "http://www.mytreedb.com/uploads/mytreedb/loader/ajax_loader_blue_48.gif",
	//       msgText: "loading..."
	//     },
	//     navSelector: "ul.pagination",
	//     nextSelector: "ul.pagination a[rel=next]",
	//     itemSelector: "#aisatu_mugen div.aisatu_mugen" /* このDOMに差し掛かった時に、次のページのロードが始まる*/
	// });
  });

// 	$("button").click(function () {
//     var text = $("#tag_it").val();
//     if(text.length == 0){
// 		  text += $(this).text();
//       }
// 			else {
// 				text += "," + $(this).text();
// 			}
//
//    $("#novel_list").val(text);
// 	 $("#tag_it").val(text);
//
// 	 });
//
//
//
// 	$( document ).ready(function(){
// 		$('#tag_it').tagit({
//      fieldName: 'novel[genre2_list]',
//      singleField: true
//    });
// });

// ポップアップでコメント
$('.tooltip-tool').tooltip({
            selector: "a[data-toggle=tooltip]"
        })
$("a[data-toggle=popover]").popover()

});

// 電光掲示板の表示　非表示
$(function() {
    $('#denkou-hyouji').click(function(){
        $('#denkou-keiji').toggle();
    });
});

// スクローラー　http://memo.ravenalala.org/scroll-to-anchor/

$(function(){
	 var headerHight = 40; //ヘッダの高さ
   $('a[href^="#scroll"]').click(function() {
      var speed = 600; // ミリ秒
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top-headerHight; //ヘッダの高さ分位置をずらす
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});

// //  ナビゲーション　上で固定するが、下にすると消える
//
// $(document).ready(function() {
// 	var $win = $(window),
// 	    $header = $('.kotei_header'),
// 	    headerHeight = $header.outerHeight(),
// 	    startPos = 0;
//
// 	$win.on('load scroll', function() {
// 		var value = $(this).scrollTop();
// 		if ( value > startPos && value > headerHeight ) {
// 			$header.css('top', '-' + headerHeight + 'px');
// 		} else {
// 			$header.css('top', '0');
// 		}
// 		startPos = value;
// 	});
// });

// ストライプてーぶる
$(document).ready(function(){
  // 偶数行の色を設定
  $('table#stripe-table tr:even').css('background-color', '#666');
  // 奇数行の色を設定
  $('table#stripe-table tr:odd').css('background-color', '#333');
});
