module ApplicationHelper

  def br(str)
    html_escape(str).gsub(/\r\n|\r|\n/, "<br />").html_safe
  end

  def mojibetu(honbun)
    length = honbun.gsub(/(\r\n|\r|\n)+|(\s|　)+/,"").length
    case length
    when 0..10000
      "超短編 #{length.to_s(:delimited)}文字"
    when 10001..30000
      "短編　 #{length.to_s(:delimited)}文字"
    when 30001..100000
      "中編　 #{length.to_s(:delimited)}文字"
    when 100000..Float::INFINITY
      "長編　 #{length.to_s(:delimited)}文字"
    end
  end



  def user_deleted(user)
    user.deleted
  end

  def get_user_name
    if current_user
      @user_about=UserAbout.find_by(user_id:current_user.id)
    end
  end

  def comment_osirase
    if current_user
      @current_user_comment=Comment.where(user_id:current_user.id)
      @current_user_comment.count
    end
  end

  def profile_img(user_about,height=70)
    if user_about.image.present?
      link_to image_tag(user_about.image.to_s,:class => "profile_image",style: "height: #{height}px"), user_user_abouts_path(user_id: user_about.user_id)
    else
      response = TwitterImage.get_response(user_about.user.uid)
      if response != "404"
        link_to twitter_profile_image_tag("#{user_about.user.uid}.jpg",:class => "profile_image",style: "height: #{height}px"),user_user_abouts_path(user_id:user_about.user_id)
      else
        link_to image_tag(user_about&.user&.image_url,:class => "profile_image",style: "height: #{height}px"),user_user_abouts_path(user_id:user_about.user_id)
      end
    end
  end

  def ssses_profile_img(user_about)
    if user_about.image.present?
      link_to image_tag(user_about.image.to_s,:class => "profile_image"), ssses_user_about_path(user_id: user_about.user_id)
    else
      response = TwitterImage.get_response(user_about.user.uid)
      if response != "404"
        link_to twitter_profile_image_tag("#{user_about.user.uid}.jpg",:class => "profile_image"),ssses_user_about_path(user_id:user_about.user_id)
      else
        link_to image_tag(user_about&.user&.image_url,:class => "profile_image"),ssses_user_about_path(user_id:user_about.user_id)
      end
    end
  end

  def shougou_kansou(user)
    return "" if user.comment_shougous.count == 0
    " <i class='fa fa-commenting-o' aria-hidden='true'></i>#{user.comment_shougous.count}:"
  end

  def shougou_sousaku(user)
    return "" if user.sousaku_shougous.count == 0
    " <i class='fa fa-pencil' aria-hidden='true'></i>#{user.sousaku_shougous.count}:"
  end

  def shougou_maturiate(user)
    return "" if user.maturiate_shougous.count == 0
    "<i class='fa fa-flag'></i>#{user.maturiate_shougous.count}:"
  end

  def user_name(user)
    "#{shougou_maturiate(user)} #{shougou_sousaku(user)} #{shougou_kansou(user)} #{user.user_about.name.truncate(14)}"

  end

  def news_detail_path(model)

    if model.class.to_s == "Novel" && controller_name == "ssses"
      sss_path(model)
    elsif model.class.to_s == "Ikebana"
      ikebana_path(model)
    end

  end

  def kirikae_header_moji
    if controller_name == "ikebanas"
      "　➡️　生け花"
    end
  end

  def kirikae_header_url
    if controller_name == "ikebanas"
      ikebanas_path
    end
  end

  def ajax_redirect_to(redirect_uri)
    { js: "window.location.replace('#{redirect_uri}');" }
  end


end
