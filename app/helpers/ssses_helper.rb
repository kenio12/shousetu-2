module SssesHelper
  def set_class(genre)
   if genre == "日常"
     return "sample-box-nichijou"
   elsif genre == "恋愛"
     return "sample-box-renai"
   elsif genre == "ジョーク"
     return "sample-box-joke"
   elsif genre == "同タイトル"
     return "sample-box-doutai"
   elsif genre == "シリーズ"
     return "sample-box-siri"
   end
  end

 def read_flag_check(read_flag)
  if !read_flag
    return "<span class='non_read'>●</span>"
  end
 end

 # def user_name(user_id)
 #   UserAbout.find_by(user_id:user_id).name
 # end


end
