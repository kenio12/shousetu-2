class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:omniauthable

 has_many :novels,dependent: :destroy
 has_many :ikebanas,dependent: :destroy

 has_one :user_about, dependent: :destroy
 has_many :comments,dependent: :destroy

 has_many :comment_notices,dependent: :destroy
 has_many :votes,dependent: :destroy,:foreign_key => "voter_id"
 has_many :comment_shougous,dependent: :destroy
 has_many :sousaku_shougous,dependent: :destroy
 has_many :maturiate_shougous,dependent: :destroy

# ユーザーモデルを通じ、ユーザーテーブルを検索。フェイスブックでのuid,フェイスブック自体と
# 同じテーブルを全て探し出し、一番最初のテーブルを取ってきて、変数に入れるメソッド
# 定義。
# もっとも、UIDとフェイスぶっとのセットは一意であり、一つ取るだけだったので、実際のところ
# find_byでもよかった。
 def self.find_for_oauth(auth)
  user = User.where(uid: auth.uid, provider: auth.provider).first

# ここは、もしテーブルにユーザーが存在しなかったら、フェイスブック上のIDをUID、フェイスブック
# をproviderに、下のプライベートで作ったダミーemailと適当な暗号を、カラムに登録する。
  unless user
    user = User.create(
      uid:      auth.uid,
      provider: auth.provider,
      email:    User.get_email(auth),
      password: Devise.friendly_token[0, 20],
      image_url: auth[:info][:image]
    )
    # logger.debug "-------------#{auth.info.gender}----------------"
    # logger.debug "-------------#{auth.info.locale}----------------"
    # logger.debug "-------------#{auth.info.age_range}----------------"

#  facebookuから情報入手様（一時中断）
    # UserAbout.create!(
    # gender: auth.extra.raw_info.gender,
    # locale: auth.extra.raw_info.location,
    # birthday: auth.extra.raw_info.age_range,
    # image: "",
    # name:"a",
    # user_id:user.id
    # )
  else
   user.deleted = false
   user.save

  end

  user
end

private

def self.get_email(auth)
      email = auth.info.email
      email = "#{auth.provider}-#{auth.uid}@example.com" if email.blank?
      email
    end

end
