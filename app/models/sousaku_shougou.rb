class SousakuShougou < ApplicationRecord
  belongs_to :user

  validates :user_id ,presence: true
  validates :shougou ,presence: true

  class << self
    def create_shougou(user)
      return if user.novels.count <= 4

      today = Time.zone.now
      tukihajime = today.beginning_of_month
      getumatu = today.end_of_month

      return if user.novels.where(:created_at=> tukihajime..getumatu).count <= 4
      return if user.sousaku_shougous.find_by(shougou:"#{today.year}年#{today.month}月の大量創作")

      user.sousaku_shougous.create(shougou:"#{today.year}年#{today.month}月の大量創作")
    end
  end
end
