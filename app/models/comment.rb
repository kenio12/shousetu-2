class Comment < ApplicationRecord
  belongs_to :novel
  belongs_to :user
  has_many :comment_notices, dependent: :destroy
  validates :body, presence: true
end
