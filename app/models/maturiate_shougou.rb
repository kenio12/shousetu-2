class MaturiateShougou < ApplicationRecord
  belongs_to :user

  validates :user_id ,presence: true
  validates :shougou ,presence: true

  class << self
    def create_shougou(user)
      today = Time.zone.now

      return if self.find_by(shougou:"#{today.year}年の大量当て")

      user.maturiate_shougous.create(shougou:"#{today.year}年の大量当て")
    end
  end
end
