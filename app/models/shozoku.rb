class Shozoku < ApplicationRecord

  has_many :user_about_shozokus
  has_many :user_abouts, through: :user_about_shozokus

end
