class Novel < ApplicationRecord
  belongs_to :user
  has_many :comments

  validates :title, presence: true ,length: { maximum: 30 }

  validate  :uniq_title, on: :create

  validates :honbun, presence: true ,length: { maximum: 5000 } ,if: :label?


  # acts_as_taggable_on :genre2s # post.label_list が追加される
  # acts_as_taggable            # acts_as_taggable_on :tags のエイリアス

  def uniq_title
    if genre == "祭り"  && Novel.exists?(genre:"祭り",title:title) && Novel.find_by(genre:"祭り",title:title).user_id != user_id
      errors.add(:title, 'と重複していますので、タイトル名を変更してください')
    end
  end

  def label?
    return false if label != 'sss'
    true
  end

# トップ画面の一覧
  def previous
  Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",genre:self.genre,past_work:self.past_work).where('toukouhiduke > ?', self.toukouhiduke).last
  end

  def next
  Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",genre:self.genre,past_work:self.past_work).where('toukouhiduke < ?', self.toukouhiduke).first
  end


# ユーザーあばうとの方
  def show_user_about_novel_previous
   Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",user_id:self.user_id,past_work:false).where('toukouhiduke > ?', self.toukouhiduke).last
  end
  def show_user_about_novel_next
  Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",user_id:self.user_id,past_work:false).where('toukouhiduke < ?', self.toukouhiduke).first
  end

  def show_user_about_past_work_novel_previous
   Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",user_id:self.user_id,past_work:true).where('toukouhiduke > ?', self.toukouhiduke).last
  end
  def show_user_about_past_work_novel_next
  Novel.order(toukouhiduke: :DESC).where(flag:true,label:"sss",user_id:self.user_id,past_work:true).where('toukouhiduke < ?', self.toukouhiduke).first
  end

  private



  class << self

  SYURUI = ["日常","歴史","時代","ファンタジー","ＳＦ","恋愛","青春","コメディ","ギャグ","推理","ホラー","アクション","バトル","社会","仕事","私小説","ギャンブル","ハードボイル","奇妙","不思議","メッセージ","アウトロー","官能","その他"]
  SSSES_SYURUI_1 = ["日常","恋愛","ジョーク","シリーズ"]
  SSSES_SYURUI_2 = ["ファンタジー","SF","日常","ホラー","ミステリー","歴史/伝奇","童話","私小説","その他"]

  SSSES_IVENT = ["イベント開催","イベント企画","オフ会"]
  SSSES_IVENT_TOUKOU = ["同タイトル","三題噺","作りかけ作品を完成に！","祭り","鍋谷さんの娘さん"]
  SSSES_IVENT_MATURINASI_TOUKOU = ["同タイトル","三題噺","作りかけ作品を完成に！","鍋谷さんの娘さん"]
  SSSES_FORAM = ["挨拶","会議","雑談"]


  def genre_list
  return SYURUI
  end

  def genre_list_ssses_1
  return SSSES_SYURUI_1
  end

  def genre_list_ssses_2
  return SSSES_SYURUI_2
  end

  def genre_list_ssses_ivent
  return SSSES_IVENT
  end

  def genre_list_ssses_ivent_toukou
  return SSSES_IVENT_TOUKOU
  end

  def genre_list_ssses_ivent_maturinasi_toukou
    return SSSES_IVENT_MATURINASI_TOUKOU
  end

  def genre_list_ssses_foram
  return SSSES_FORAM
  end

  end

end
