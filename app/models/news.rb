class News < ApplicationRecord
  enum category: { zentai: 0, novel: 10, ssses: 11, ikebana: 20 }

  scope :new_news, -> { where("expired_at >= ?",DateTime.now).or(News.where(expired_at:nil)) }
  scope :old_news, -> { where("expired_at < ?",DateTime.now) }
  scope :order_d, -> { order(created_at: :DESC) }
  scope :ikebana, -> { where(category: ['ikebana', 'zentai']) }
  scope :ssses, -> { where(category: ['ssses', 'zentai']) }

end
