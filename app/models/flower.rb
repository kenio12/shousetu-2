class Flower < ApplicationRecord
  has_many :ikebana_flowers
  has_many :ikebanas, through: :ikebana_flowers

  mount_uploader :image, ImageUploader

end
