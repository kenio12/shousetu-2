class Ikebana < ApplicationRecord

  belongs_to :user

  has_many :ikebana_flowers
  has_many :flowers, through: :ikebana_flowers

  mount_uploader :image, ImageUploader

end
