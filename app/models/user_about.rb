class UserAbout < ApplicationRecord

  mount_uploader :image, ImageUploader

  require 'jp_prefecture'
  belongs_to :user
  validates :name, presence: true, uniqueness: true,length: { in: 1..20 }

  has_many :user_about_shozokus
  has_many :shozokus, through: :user_about_shozokus

end
