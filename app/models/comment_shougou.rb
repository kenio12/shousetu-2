class CommentShougou < ApplicationRecord
  belongs_to :user

  validates :user_id ,presence: true
  validates :shougou ,presence: true

  class << self
    def create_shougou(user)
      return if user.comments.count <= 8

      today = Time.zone.now
      tukihajime = today.beginning_of_month
      getumatu = today.end_of_month

      return if user.comments.where(:created_at=> tukihajime..getumatu).count <= 8
      return if user.comment_shougous.find_by(shougou:"#{today.year}年#{today.month}月の大量感想")

      user.comment_shougous.create(shougou:"#{today.year}年#{today.month}月の大量感想")
    end
  end
end
