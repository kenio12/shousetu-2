class Message < ApplicationRecord
  # メッセージを保存したら、selfであるメッセージの内容を引数にして、ジョブ（MessageBroadcastJob）を起動してね。
  # MessageBroadcastJobと言うのはmessage_broadcast_job.rbのこと。
    after_create_commit { MessageBroadcastJob.perform_later self }

    def your_id(user)
       return_id = 0
       if sender_id == user.id
       return_id = receiver_id
       else
       return_id =  sender_id
       end
       return_id
    end

end
