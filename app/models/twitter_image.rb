require 'net/http'
require 'uri'


class TwitterImage
  def self.get_response(uid)

    image_url = "http://res.cloudinary.com/hphnb51mu/image/twitter/#{uid}.jpg"

    uri = URI.parse(image_url)

    res = Net::HTTP.start(uri.host, uri.port) { |http| http.get(uri.path) }

    res.code
  end
end
