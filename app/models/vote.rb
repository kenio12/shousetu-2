class Vote < ApplicationRecord
  belongs_to :voter,class_name: "User",foreign_key:"vote_id"
  belongs_to :writer,class_name: "User",foreign_key:"writer_id"
end
