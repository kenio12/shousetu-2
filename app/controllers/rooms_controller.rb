class RoomsController < ApplicationController
  #  うるまさんいない時　9/17
 before_action :set_read_flag
 before_action :get_message_notice



  def show
    # logger.debug "-----------#{current_user.id}-------------"
    # logger.debug "-----------#{params[:user_id]}-------------"
    if current_user.id.to_s == params[:user_id] || !User.exists?(id: params[:user_id])
      redirect_to ssses_path and return
    end



    @my_user_about = current_user.user_about
    @you_user_about = UserAbout.find_by(user_id:params[:user_id])

    if @my_user_about.image.present?
      @my_image_url = @my_user_about.image.to_s
    elsif @my_user_about.user.image_url.present?
      @my_image_url = @my_user_about.user.image_url
    else
      @my_image_url = "ikebana.png"
    end

    if @you_user_about.image.present?
      @you_image_url = @you_user_about.image.to_s
    elsif @you_user_about.user.image_url.present?
      @you_image_url = @you_user_about.user.image_url
    else
      @you_image_url = "ikebana.png"
    end


    @messages = []
    @messages << Message.where(sender_id:current_user,receiver_id:params[:user_id]).pluck(:created_at, :sender_id, :content)
    @messages << Message.where(sender_id:params[:user_id],receiver_id:current_user).pluck(:created_at, :sender_id, :content)
    logger.debug "--------------#{@messages.flatten(1).sort!}-----------------"
    @messages = @messages.flatten(1).sort!



    gon.sender_id = current_user.id
    gon.receiver_id = params[:user_id]

    gon.my_image_url = @my_image_url
    gon.you_image_url = @you_image_url

    if params[:label] == "ssses"
      render layout: 'short_short_story'
    end

  end

#  うるまさんいない時　9/17
  private

  def set_read_flag
    #  うるまさんいない時　9/17
    check_messages = Message.where(sender_id:params[:user_id],receiver_id:current_user.id, read_flag: false)
    check_messages.each do |message|
    message.read_flag = true
    message.save
    end
  end

  def get_message_notice
    if user_signed_in?
      if Message.where(receiver_id:current_user.id,read_flag:false).present?
        @message_notice = true
      else
        @message_notice = false
      end
              logger.debug "------------------#{@message_notice}------------------------"
    end
  end

end
