class NovelsController < ApplicationController

  before_action :set_novel, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! , only:[:new,:create,:edit,:update,:destroy]
  before_action :nanasi_user_about! , only:[:new,:create,:edit,:update,:destroy]
  before_action :tanin_ijiri!, only:[:edit,:update,:destroy]
  # GET /novels
  # GET /novels.json

  def short_short_story
    @novels = Novel.where(flag:true).order("created_at DESC").page(params[:page])
    @users = []
    @comments = []
    i=0
    @novels.each do |novel|
    @users[i]=User.find(novel.user_id)
    @comments[i] = Comment.where(novel_id:novel.id)
    i=i+1
    end
    @shousetukansous = Shousetukansou.all.order("created_at DESC").page(params[:page])
  end

  def index
    # @novels = Novel.all
    @novels = Novel.where(flag:true).order("created_at DESC").page(params[:page])
    @users = []
    @comments = []
    i=0
    @novels.each do |novel|
    @users[i]=User.find(novel.user_id)
    @comments[i] = Comment.where(novel_id:novel.id)
    i=i+1
    end
    @shousetukansous = Shousetukansou.all.order("created_at DESC").page(params[:page])
  end

  # GET /novels/1
  # GET /novels/1.json
  def show
    if @novel.flag
    @user = User.find(@novel.user_id)
    @user_about=UserAbout.find_by(user_id:@user.id)
    @comments = @novel.comments.all
    @comment  = @novel.comments.build(user_id: current_user.id) if current_user
    else
    redirect_to novels_path
    end
  end

  # GET /novels/new
  def new
    @novel = Novel.new
    #  if params[:preview_button] || !@novel.save
    #  @user = User.find(current_user.id)
    #  @comment = nil
    #  @user_about = UserAbout.find_by(user_id:@user.id)
    #  end
  end

  # GET /novels/1/edit
  def edit
  end

  # POST /novels
  # POST /novels.json
  def create
    @novel = Novel.new(novel_params)
    if params[:preview_button]
       @user = User.find(current_user.id)
       @user_about = UserAbout.find_by(user_id:@user.id)
       @comment = nil
       render :new and return
    end

    if params[:sitagaki]
      @novel.flag = false
    else
      @novel.flag = true
      @novel.toukouhiduke = DateTime.now()
    end

    respond_to do |format|
      if @novel.save
        if @novel.flag == false
          format.html { redirect_to user_user_abouts_path(current_user.id), notice: '下書き保存しました！' }
        else
          format.html { redirect_to novels_path notice: '小説を投稿しました！　おつかれさまです。' }
        end
        format.json { render :show, status: :created, location: @novel }
      else
        format.html { render :new }
        format.json { render json: @novel.errors, status: :unprocessable_entity }
      end
    end

   end


  # PATCH/PUT /novels/1
  # PATCH/PUT /novels/1.json
  def update
    # @novel = Novel.new(novel_params)
    if params[:preview_button]
       @user = User.find(current_user.id)
       @user_about = UserAbout.find_by(user_id:@user.id)
       @comment = nil
      #  logger.debug "-------------------#{params[:novel][:honbun]}---------------------------"
       @novel.genre = params[:novel][:genre]
       @novel.title = params[:novel][:title]
       @novel.honbun = params[:novel][:honbun]
       render :edit and return
    end


    if params[:sitagaki]
      @novel.flag = false
    else
      @novel.flag = true
      if @novel.toukouhiduke.blank?
      @novel.toukouhiduke = DateTime.now()
      end
    end

    respond_to do |format|
      if @novel.update(novel_params)
        if @novel.flag == false
          format.html { redirect_to user_user_abouts_path(current_user.id), notice: '下書き保存しました！' }
        else
          format.html { redirect_to novels_path, notice: '小説を修正しました！　おつかれさまです。' }
        end
        format.json { render :show, status: :created, location: @novel }
      else
        format.html { render :new }
        format.json { render json: @novel.errors, status: :unprocessable_entity }
      end
    end
    # respond_to do |format|
    #   if @novel.update(novel_params)
    #     format.html { redirect_to @novel, notice: 'Novel was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @novel }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @novel.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /novels/1
  # DELETE /novels/1.json
  def destroy
    @novel.destroy
    respond_to do |format|
      format.html { redirect_to novels_url, notice: 'Novel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_novel
      @novel = Novel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def novel_params
      params.require(:novel).permit(:genre,:title, :sakushamei,:honbun,:user_id)
    end

    def nanasi_user_about!
      if current_user.user_about.nil?
        redirect_to(user_user_abouts_path(:user_id => current_user.id))
      end
    end

    def tanin_ijiri!

       if @novel.user_id != current_user.id
       redirect_to(@novel)
       end
    end


end
