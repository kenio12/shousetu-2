class UserAboutsController < ApplicationController
    before_action :set_user_about, only: [:create, :edit, :update, :show]

  def index
    # @user = User.find_by(id:@user_about.user_id)
    @user = User.find_by(id:params[:user_id])

    @user_about = UserAbout.find_by(user_id:params[:user_id])

      if @user_about.blank?
      @user_about = UserAbout.new
      end

    @novels = Novel.where(user_id:@user_about.user_id).where(flag:true).order("created_at DESC").page(params[:page])
    @novels_tukurikake = Novel.where(user_id:@user_about.user_id).where(flag:false).order("created_at DESC").page(params[:page])

    @users = Array.new(@novels.count, @user)

    i=0
    @comments = []
     @novels.each do |novel|
     @comments[i] = Comment.where(novel_id:novel.id)
     i += 1
     end

      i=0
      @comments_tukurikake = []
      @novels_tukurikake.each do |novel_tukurikake|
      @comments_tukurikake[i] = Comment.where(novel_id:novel_tukurikake.id)
      i += 1
      end

      @shousetukansous = Shousetukansou.where(user_id:@user_about.user_id).order("created_at DESC").page(params[:page])

  end

  def create
     @user_about = UserAbout.new(user_about_params)
     @user_about.user_id=params[:user_id]

    respond_to do |format|
      if @user_about.save
        format.html { redirect_to user_user_abouts_path(:user_id => @user_about.user_id), notice: "名前登録成功です！" }
        format.json { render :index, status: :created, location: @user_about }
      else
        @user = User.find_by(id:params[:user_id])
        format.html { render :index, alert: '名前登録失敗です'}
        format.json { render json: @user_about.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    respond_to do |format|
      if @user_about.update(user_about_params)
        format.html { redirect_to user_user_abouts_path(:user_id => @user_about.user_id), notice: "プロフィール更新完了です！" }
        format.json { render :index, status: :created, location: @user_about }
      else
        @user = User.find_by(id:params[:user_id])
        format.html { render :index, alert: 'プロフィール更新に失敗しました'}
        format.json { render json: @user_about.errors, status: :unprocessable_entity }
      end
    end

  end

  def show
     @user = User.find_by(id:params[:user_id])
     @user_about = UserAbout.find_by(user_id:params[:user_id])

     @novels = Novel.where(user_id:@user_about.user_id).where(flag:true).order("created_at DESC").page(params[:page])

     # flag がtrue の数だけ配列を作っている。　@user　は　うめもとさん自身の情報が入っている。　うめもとさん情報が入った複数の配列が入っている。
     @users = Array.new(@novels.count, @user)

     i=0
     @comments = []
      @novels.each do |novel|
      @comments[i] = Comment.where(novel_id:novel.id)
      i += 1
      end

      @shousetukansous = Shousetukansou.where(user_id:@user_about.user_id).order("created_at DESC").page(params[:page])

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_about
      @user_about = UserAbout.find_by(user_id:params[:user_id])
    end


    def user_about_params
      # params.fetch(:user_about, {}).permit(:name)
        params.require(:user_about).permit(:name,:gender,:birthday,:locale,:user_id,:image)
    end

end
