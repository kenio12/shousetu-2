class VotesController < ApplicationController

  before_action :authenticate_user! , only:[:new,:create,:edit,:update,:destroy]
  before_action :set_vote, only: [:show, :edit, :destroy]
  layout 'short_short_story' 



  def index
    @votes = Vote.all
  end

  def show
  end

  def new
    @votes = Vote.new
  end

  def create
    @votes = votes_params.map do |vote_param|
      vote = Vote.new(vote_param)
      unless vote.save!
        redirect_to ssses_path, notice: '投票に不備があり、投票できませんでした。' and return
      end
      vote
    end

    respond_to do |format|
      format.html { redirect_to ssses_path, notice: '投票出来ました' }
      format.json { render :show, status: :created, location: @votes }
    end
  end

  def edit
  end

  def update
    @votes = votes_params.map do |id,vote_param|
      vote = Vote.find(id)
      unless vote.update_attributes(vote_param)
        redirect_to ssses_path, notice: '不備があり、投票を編集できませんでした。' and return
      end
      vote
    end

    respond_to do |format|
        format.html { redirect_to ssses_path, notice: '投票編集が完了しました' }
        format.json { render :show, status: :created, location: @vote }
    end
  end

  def destroy
    @vote.destroy
    respond_to do |format|
      format.html { redirect_to ssses_url, notice: '投票を消しました' }
      format.json { head :no_content }
    end
  end

  def result
    user_id = params[:user_id]
    @votes = Vote.where(voter_id: user_id).order(:novel_id)
    @user = User.find(user_id)
  end

  private

  def set_vote
    @vote = Vote.find(params[:id])
  end

  def votes_params
    # params.require(:votes).permit(:voter_id,:novel_id,:writer_id)
    params.permit(votes: [:voter_id,:novel_id,:writer_id])[:votes]
  end

  # private
  #   def items_params
  #     params.permit(items: [:name, :price])[:items]
  #   end

end
