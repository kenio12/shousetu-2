class IkebanasController < ApplicationController
  before_action :set_ikebana, only: [:show, :edit, :update, :destroy]

  # GET /ikebanas
  # GET /ikebanas.json
  def index
    @ikebanas = Ikebana.all.order(created_at: :DESC)
    @now_news = News.ikebana.new_news.order_d
    @old_news = News.ikebana.old_news.order_d
    @params = "ikebana"
    @ivents =Ikebana.where(genre:"イベント開催").order(created_at: :DESC)

  end

  # GET /ikebanas/1
  # GET /ikebanas/1.json
  def show
  end

  # GET /ikebanas/new
  def new
    # binding.pry
    @ikebana = Ikebana.new
    @flower_contents = [["ひまわり", "ひまわり"], ["あじさい", "あじさい"]]
    gon.flower_contents = [["ひまわり", "ひまわり"], ["あじさい", "あじさい"]]
  end

  # GET /ikebanas/1/edit
  def edit
    @flower_contents = [["ひまわり", "ひまわり"], ["あじさい", "あじさい"]]
    gon.flower_contents = [["ひまわり", "ひまわり"], ["あじさい", "あじさい"]]
    gon.ikebana = @ikebana
  end

  # POST /ikebanas
  # POST /ikebanas.json
  def create
    @ikebana = Ikebana.new(ikebana_params)
    @ikebana.user_id = current_user.id

#  ターミナルで見れる
    # binding.pry
    respond_to do |format|
      if @ikebana.save!
        params[:ikebana][:flowers].split(",").each do |flower_name|
          # binding.pry
          flower = Flower.find_by(name: flower_name)
          IkebanaFlower.create(ikebana_id:@ikebana.id,flower_id:flower.id)
        end

        format.html { redirect_to @ikebana, notice: 'Ikebana was successfully created.' }

        format.js { render js: "window.location = '#{ikebana_path(@ikebana)}'" }

        format.json { render :show, status: :created, location: @ikebana }

      else
        format.html { render :new }
        format.json { render json: @ikebana.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ikebanas/1
  # PATCH/PUT /ikebanas/1.json
  def update
    respond_to do |format|
      if @ikebana.update(ikebana_params)
        @ikebana.flowers.destroy_all
        params[:ikebana][:flowers].split(",").each do |flower_name|
          # binding.pry
          flower = Flower.find_by(name: flower_name)
          IkebanaFlower.create(ikebana_id:@ikebana.id,flower_id:flower.id)
        end
        format.html { redirect_to @ikebana, notice: 'Ikebana was successfully updated.' }
        format.json { render :show, status: :ok, location: @ikebana }
        format.js { render js: "window.location = '#{ikebana_path(@ikebana)}'" }
      else
        format.html { render :edit }
        format.json { render json: @ikebana.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ikebanas/1
  # DELETE /ikebanas/1.json
  def destroy
    @ikebana.destroy
    respond_to do |format|
      format.html { redirect_to ikebanas_url, notice: 'Ikebana was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ikebana
      @ikebana = Ikebana.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ikebana_params
    params.require(:ikebana).permit(:user_id, :title, :image,:message,:genre)
    end


end
