class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  after_action  :store_location
  def store_location
    if (request.fullpath != "/users/sign_in" &&
      request.fullpath != "/users/sign_up" &&
      request.fullpath !~ Regexp.new("\\A/users/password.*\\z") &&
      request.fullpath != "/users/sign_out" &&
      !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath
    end
  end

  # after_sign_in_path_for のメソッドはredirect_to は不要。そんなメソッド。これ自体が飛ばす機能つき。
  def after_sign_in_path_for(resource)
    if current_admin_user
      admin_url
    else
      # check = UserAbout.find_by(user_id: current_user.id)
      if session[:previous_url].include?("/ssses")
        if current_user.user_about.nil?
          ssses_user_about_path(:user_id => current_user.id)
        else
          session[:previous_url]
        end
      else
        user_user_abouts_path(:user_id => current_user.id)
      end
    end
  end

  def after_sign_out_path_for(resource)
    request.referrer
    # if session[:previous_url].include?("/ssses")
    #  session[:previous_url]
    #  else
    #  root_path
    # end
  end

end
