class ShousetukansousController < ApplicationController
  before_action :set_shousetukansou, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! , only:[:new,:create,:edit,:update,:destroy]
  before_action :nanasi_user_about! , only:[:new,:create,:edit,:update,:destroy]
  before_action :honninkakunin_user! , only:[:edit,:update,:destroy]

  # GET /shousetukansous
  # GET /shousetukansous.json
  def index
    @shousetukansous = Shousetukansou.all.order("created_at DESC").page(params[:page])
  end

  # GET /shousetukansous/1
  # GET /shousetukansous/1.json
  def show
  @user = User.find(@shousetukansou.user_id)
  @user_about = UserAbout.find_by(user_id:@user.id)
  end

  # GET /shousetukansous/new
  def new
    @shousetukansou = Shousetukansou.new
    @label = params[:label]
  end

  # GET /shousetukansous/1/edit
  def edit
  end

  # POST /shousetukansous
  # POST /shousetukansous.json
  def create
    @shousetukansou = Shousetukansou.new(shousetukansou_params)

    respond_to do |format|
      if @shousetukansou.save
        format.html { redirect_to @shousetukansou, notice: 'Shousetukansou was successfully created.' }
        format.json { render :show, status: :created, location: @shousetukansou }
      else
        format.html { render :new }
        format.json { render json: @shousetukansou.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shousetukansous/1
  # PATCH/PUT /shousetukansous/1.json
  def update
    respond_to do |format|
      if @shousetukansou.update(shousetukansou_params)
        format.html { redirect_to @shousetukansou, notice: 'Shousetukansou was successfully updated.' }
        format.json { render :show, status: :ok, location: @shousetukansou }
      else
        format.html { render :edit }
        format.json { render json: @shousetukansou.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shousetukansous/1
  # DELETE /shousetukansous/1.json
  def destroy
    @shousetukansou.destroy
    respond_to do |format|
      format.html { redirect_to shousetukansous_url, notice: 'Shousetukansou was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shousetukansou
      @shousetukansou = Shousetukansou.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shousetukansou_params
      params.require(:shousetukansou).permit(:image, :shuppansha, :chosha, :shoseki, :comment, :hyouka, :user_id,:label)
    end

    def nanasi_user_about!
      if current_user.user_about.nil?
        redirect_to(user_user_abouts_path(:user_id => current_user.id))
      end
    end

    def honninkakunin_user!
      if current_user.id != @shousetukansou.user_id
         redirect_to @shousetukansou
      end
    end

end
