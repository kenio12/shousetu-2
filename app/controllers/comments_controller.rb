class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
    @novel = Novel.new
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to novel_path(@comment.novel_id), notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
      else

        @novel = Novel.find(params[:comment][:novel_id])
        @user = User.find(@novel.user_id)
        @user_about=UserAbout.find_by(user_id:@user.id)
        @comments = @novel.comments.all
        # @comment  = @novel.comments.build(user_id: current_user.id) if current_user


        format.html { render "novels/show" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    comment_novel_id = @comment.novel_id
    @comment.destroy
    respond_to do |format|
     if params[:docchi] == "sss"
       format.html { redirect_to sss_path(comment_novel_id), notice: 'Comment was successfully destroyed.' }
       format.json { head :no_content }
     elsif params[:docchi] == "sss_user_about"
       format.html { redirect_to ssses_show_user_about_novel_path(comment_novel_id), notice: 'Comment was successfully destroyed.' }
       format.json { head :no_content }
     else
      format.html { redirect_to novel_path(@comment.novel_id), notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
     end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:user_id, :novel_id, :body)
    end
end
