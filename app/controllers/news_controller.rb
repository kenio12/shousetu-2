class NewsController < ApplicationController


  before_action :authenticate_admin_user! , only:[:new,:create,:edit,:update,:destroy]
  before_action :set_new, only: [:show, :edit, :update, :destroy]

  layout :layout_select

  def layout_select
    case params[:from]
    when "ssses"
      'short_short_story'
    end
  end



  def index
    @news = News.all
  end

  def show
  end

  def new
    @new = News.new
  end

  def create
    @new = News.new(news_params)

    respond_to do |format|
      if @new.save
        format.html { redirect_to ssses_path, notice: 'お知らせ出来ました' }
        format.json { render :show, status: :created, location: @new }
      else
        format.html { render :new }
        format.json { render json: @new.errors, status: :unprocessable_entity }
      end
    end

  end

  def edit
  end

  def update
    respond_to do |format|
      if @new.update(news_params)
          format.html { redirect_to ssses_path, notice: 'お知らせ編集完了' }
          format.json { render :show, status: :created, location: @new }
      else
        format.html { render :edit }
        format.json { render json: @new.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @new.destroy
    respond_to do |format|
      format.html { redirect_to ssses_url, notice: 'お知らせ消した' }
      format.json { head :no_content }
    end
  end

  private

    def set_new
      @new = News.find(params[:id])
    end

    def news_params
      params.require(:news).permit(:title,:body,:expired_at,:category)
    end

end
