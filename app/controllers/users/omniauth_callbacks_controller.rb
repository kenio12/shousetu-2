class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

# ここは下のコールバックから読むべきだ

  def facebook
    callback_from :facebook
  end

  def twitter
    callback_from :twitter
  end

  private
# callback_fromというメソッド定義。
# まず、引数がフェイスブックであるのなら、そのフェイスブックを文字化する。

  def callback_from(provider)
    provider = provider.to_s
    # 次に、モデルで作成したメソッドを用いて、HTTP上のヘッダーより、個人データ部分を取り、
    # それを元に、テーブルから同じアイディーとフェイスブックがあるか探し、見つけたものを
    # インスタンス変数である@userに代入する。
    @user = User.find_for_oauth(request.env['omniauth.auth'])
# 代入した個人データがテーブルに保存されている場合、フラッシュを用いて成功した旨を他言語対応
# で表示し、音声でも、伝える。
# 保存済みのデータか調べて、そうなら、ユーザーにdevise上での、つまりこのサイトでの立ち入りを認可する。でもって@userにリダイレクトする。
    if @user.persisted?
      flash[:notice] = I18n.t('devise.omniauth_callbacks.success', kind: provider.capitalize)
      sign_in_and_redirect @user, event: :authentication
# そうでない時、つまり変数ユーザーが保存できていないとき、つまりフェイスブック上でユーザーが
# 存在しなかった時になるのかな？　そのユーザー情報をセッションに入れてやった上で
# 新規登録画面に遷移させる。
    else
      session["devise.#{provider}_data"] = request.env['omniauth.auth']
      redirect_to new_user_registration_url
    end
  end
end
