class SssesController < ApplicationController

  before_action :set_novel, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! , only:[:new,:create,:edit,:update,:destroy,:create_user_about,:update_user_about,:comment_create,:comment_update]
  before_action :nanasi_user_about! , only:[:new,:create,:edit,:update,:destroy]
  before_action :tanin_ijiri!, only:[:edit,:update,:destroy]

  before_action :check_notice,only:[:show]
  before_action :get_notices

  before_action :get_message_notice
  before_action :get_deadline
  layout 'short_short_story' #これで、ショートとのレイアウトが呼び込まれる
  # GET /novels
  # GET /novels.json

  def index

    @users = []
    @comments = []

    @now_news = News.ssses.new_news.order_d
    @old_news = News.ssses.old_news.order_d
    puts  "--------------#{@now_news.inspect}-----------"
    puts  "--------------#{@old_news.inspect}-----------"

    @params = "ssses"

  # うるまさんがいない時に11/19
  # if @today < @happyou
  #   @novels = Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_maturinasi_toukou)).order(toukouhiduke: :DESC)
  # else
    @novels = Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_toukou)).order(toukouhiduke: :DESC)
  # end

    @chokkin_novels = @novels.first(5)


    @kaigi =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"会議",past_work:false).order(toukouhiduke: :DESC).first(5)
    @zatudan =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"雑談",past_work:false).order(toukouhiduke: :DESC).first(5)


    @sinsaku_nichijou_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"日常",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_nichijou_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"日常",past_work:true).order(toukouhiduke: :DESC).first(10)

    @sinsaku_renai_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"恋愛",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_renai_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"恋愛",past_work:true).order(toukouhiduke: :DESC).first(10)

    @sinsaku_joke_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"ジョーク",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_joke_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"ジョーク",past_work:true).order(toukouhiduke: :DESC).first(10)

    @sinsaku_siriz_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"シリーズ",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_siriz_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"シリーズ",past_work:true).order(toukouhiduke: :DESC).first(10)

    @sinsaku_doutitle_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"同タイトル",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_doutitle_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"同タイトル",past_work:true).order(toukouhiduke: :DESC).first(10)

    @sinsaku_sandaibanasi_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"三題噺",past_work:false).order(toukouhiduke: :DESC).first(5)
    @tukurikake_novels=Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"作りかけ作品を完成に！",past_work:false).order(toukouhiduke: :DESC).first(5)
    @sinsaku_maturi_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"祭り",past_work:false).order(toukouhiduke: :DESC).first(5)

    @kotoshino_maturi_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"祭り",past_work:false,toukouhiduke:@deadline.prev_month(4)..@deadline.next_month(4)).order(toukouhiduke: :DESC)

    @nabetani_novels = Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"鍋谷さんの娘さん",past_work:false).order(toukouhiduke: :DESC).first(5)

    @ivent =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"イベント開催",past_work:false).order(toukouhiduke: :DESC).first(5)
    @ivents =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"イベント開催",past_work:false).order(toukouhiduke: :DESC)

    @kikaku =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"イベント企画",past_work:false).order(toukouhiduke: :DESC).first(5)
    @offkai =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"オフ会",past_work:false).order(toukouhiduke: :DESC).first(5)

    # 無限スクルールの変数
    @aisatu =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"挨拶",past_work:false).includes(:comments).order(toukouhiduke: :DESC).page(params[:page]).per(5)
    @aisatu_comments=[]
    @aisatu.each do |aisatu|
      if aisatu.comments.present?
        aisatu.comments.each do |comment|

          if comment.created_at.year == Date.today.year && (comment.created_at.month == Date.today.last_month.month || comment.created_at.month == Date.today.month)
            @aisatu_comments.unshift(comment)
          end
        end
      end
    end
    @sinsaku_nichijou_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"日常",past_work:false).order(toukouhiduke: :DESC).first(5)
    @past_work_nichijou_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"日常",past_work:true).order(toukouhiduke: :DESC).first(5)
    @joke_users=[]
    @joke_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"ジョーク").order(past_work: :ASC,toukouhiduke: :DESC).first(2)
    @joke_novels
    i=0
    @joke_novels.each do |novel|
      @joke_users[i]=User.find(novel.user_id)
      i=i+1
    end
    @siriz_users=[]
    @siriz_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"シリーズ").order(past_work: :ASC,toukouhiduke: :DESC).first(2)
    i=0
    @siriz_novels.each do |novel|
      @siriz_users[i]=User.find(novel.user_id)
      i=i+1
    end
    @doutaitle_users=[]
    @doutaitle_novels =Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:"同タイトル").order(past_work: :ASC,toukouhiduke: :DESC).first(3)
    i=0
    @doutaitle_novels.each do |novel|
      @doutaitle_users[i]=User.find(novel.user_id)
      # @comments[i] = Comment.where(novel_id:novel.id)
      i=i+1
    end
    @shozokus = Shozoku.where(name:"超短編小説会").order(created_at: :ASC)

    # @votes = Vote.where

    # 投票者をとってきた
     voter_ids = Vote.where('created_at > ?',Date.new(@today.year,1,11)).pluck(:voter_id).uniq
     puts "--------------#{voter_ids.inspect}-----------"
     @ranking = {}

     voter_ids.each do |id|
       count =0
       votes = Vote.where('created_at > ?',Date.new(@today.year,1,11)).where(voter_id:id)
       votes.each do |vote|
         novel = Novel.find(vote.novel_id)
         if novel.user_id == vote.writer_id
           count += 1
         end
       end
       @ranking[id] = count
     end

     @ranking = @ranking.sort_by{ |_, v| -v }.to_h
     if @ranking.present?
       MaturiateShougou.create_shougou(User.find(@ranking.to_a.last[0]))
       logger.debug"-------------#{@ranking.to_a.last[0]}--------------------"
     end
  end

  def ichiran

    @sinsaku_novels = Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:params[:genre],past_work:false).order(toukouhiduke: :DESC)
    @past_work_novels = Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:params[:genre],past_work:true).order(toukouhiduke: :DESC)

    # @users = []
    # i=0
    # @novels.each do |novel|
    # @users[i]=User.find(novel.user_id)
    # i=i+1
    # end

  end

  def chokkin_novels_zenran
      # うるまさんがいない時に11/19
    # if @today < @happyou
    #   @novels = Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_maturinasi_toukou)).order(toukouhiduke: :DESC)
    # else
      @chokkin_novels = Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.includes(user: :user_about).where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_toukou)).order(toukouhiduke: :DESC).page(params[:page]).per(10)
    # end

  end


  # GET /novels/1
  # GET /novels/1.json
  def show

    if @novel.flag
      @user = User.find(@novel.user_id)
      @user_about=UserAbout.find_by(user_id:@user.id)
      @comments = @novel.comments.order(created_at: :ASC)
      if params[:comment_id].present?
        @comment = Comment.find(params[:comment_id])
      else
        @comment  = @novel.comments.build(user_id: current_user.id) if current_user
      end
    else
      redirect_to ssses_path
    end
  end

  # GET /novels/new
  def new
    @novel = Novel.new
    #  if params[:preview_button] || !@novel.save
    #  @user = User.find(current_user.id)
    #  @comment = nil
    #  @user_about = UserAbout.find_by(user_id:@user.id)
    #  end
  end

  # GET /novels/1/edit
  def edit
  end

  # POST /novels
  # POST /novels.json
  def create
    @novel = Novel.new(sss_params)


    if params[:preview_button]
      @user = User.find(current_user.id)
      @user_about = UserAbout.find_by(user_id:@user.id)
      @comment = nil
      render :new and return
    end

    if params[:sitagaki]
      @novel.flag = false
    else
      @novel.flag = true
      @novel.toukouhiduke = DateTime.now()
    end

    @novel.label = "sss"

    respond_to do |format|
      if @novel.save

        if @novel.flag == false
          format.html { redirect_to ssses_user_about_path+"?user_id=+#{@current_user.user_about.user_id}", notice: '下書き保存しました！' }
        elsif Novel.genre_list_ssses_ivent.include?(@novel.genre)
          format.html { redirect_to ssses_path ,notice: "イベントのトピックを作成しました！　おつかれさまです。" }
        elsif Novel.genre_list_ssses_foram.include?(@novel.genre)
          format.html { redirect_to ssses_path ,notice: "フォーラムのトピックを作成しました！　おつかれさまです。" }
        else
          SousakuShougou.create_shougou(current_user)
          format.html { redirect_to ssses_path ,notice: "小説を投稿しました！　おつかれさまです。" }
        end
        format.json { render :show, status: :created, location: @novel }
      else
        format.html { render :new }
        format.json { render json: @novel.errors, status: :unprocessable_entity }
      end
    end

  end


  # PATCH/PUT /novels/1
  # PATCH/PUT /novels/1.json
  def update
    # @novel = Novel.new(novel_params)
    if params[:preview_button]
      @user = User.find(current_user.id)
      @user_about = UserAbout.find_by(user_id:@user.id)
      @comment = nil
      #  logger.debug "-------------------#{params[:novel][:honbun]}---------------------------"
      @novel.genre = params[:novel][:genre]
      @novel.title = params[:novel][:title]
      @novel.honbun = params[:novel][:honbun]
      @novel.atogaki = params[:novel][:atogaki]
      #  @novel.label = params[:novel][:label]
      render :edit and return
    end


    if params[:sitagaki]
      @novel.flag = false
    else
      @novel.flag = true
      if @novel.toukouhiduke.blank?
        @novel.toukouhiduke = DateTime.now()
      end
    end

    respond_to do |format|
      if @novel.update(sss_params)
        if @novel.flag == false
          format.html { redirect_to ssses_user_about_path+"?user_id=+#{current_user.user_about.user_id}", notice: '下書き保存しました！' }
        else
          format.html { redirect_to ssses_path, notice: '小説を修正しました！　おつかれさまです。' }
        end
        format.json { render :show, status: :created, location: @novel }
      else
        format.html { render :new }
        format.json { render json: @novel.errors, status: :unprocessable_entity }
      end
    end
    # respond_to do |format|
    #   if @novel.update(novel_params)
    #     format.html { redirect_to @novel, notice: 'Novel was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @novel }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @novel.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /novels/1
  # DELETE /novels/1.json
  def destroy
    if @novel.flag == false
      @novel.destroy
      respond_to do |format|
        format.html { redirect_to ssses_user_about_path(user_id:current_user.user_about.user_id), notice: '消えたよ' }
        format.json { head :no_content }
      end
    else
      @novel.destroy
      respond_to do |format|
        format.html { redirect_to ssses_url, notice: '消えたよ' }
        format.json { head :no_content }
      end
    end
  end

  def shousetukansous
    @shousetukansous = Shousetukansou.where(label:"sss").order("created_at DESC").page(params[:page])
  end

  def user_about
    @user = User.find(params[:user_id])

    @user_about = UserAbout.find_by(user_id:@user.id)

    if @user_about.blank?
      @user_about = UserAbout.new
    end

    @novels = Novel.where(user_id:@user_about.user_id).where(flag:true).order("created_at DESC").page(params[:page])

    if @today < @happyou && current_user != @user
      @novels = Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_maturinasi_toukou)).order(toukouhiduke: :DESC)
    else
      @novels = Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_1).or(Novel.where(flag:true,label:"sss",genre:Novel.genre_list_ssses_ivent_toukou)).order(toukouhiduke: :DESC)
    end

    if @today < @happyou && current_user != @user
      @sinsaku_novels =Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:false,genre:Novel.genre_list_ssses_1).or(Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:false,genre:Novel.genre_list_ssses_ivent_maturinasi_toukou)).order(toukouhiduke: :DESC)
      @past_work_novels =Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:true).order(toukouhiduke: :DESC)
    else
      @sinsaku_novels =Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:false,genre:Novel.genre_list_ssses_1).or(Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:false,genre:Novel.genre_list_ssses_ivent_toukou)).order(toukouhiduke: :DESC)
      @past_work_novels =Novel.where(user_id:@user_about.user_id).where(flag:true,label:"sss",past_work:true).order(toukouhiduke: :DESC)
    end


    @novels_tukurikake = Novel.where(user_id:@user_about.user_id).where(flag:false).order("created_at DESC").page(params[:page])

    @users = Array.new(@novels.count, @user)

    i=0
    @comments = []
    @novels.each do |novel|
      @comments[i] = Comment.where(novel_id:novel.id)
      i += 1
    end

    i=0
    @comments_tukurikake = []
    @novels_tukurikake.each do |novel_tukurikake|
      @comments_tukurikake[i] = Comment.where(novel_id:novel_tukurikake.id)
      i += 1
    end

    @shousetukansous = Shousetukansou.where(user_id:@user_about.user_id).order("created_at DESC").page(params[:page])

    # うるまさんがいない時に実装
    @rekisi_novels =@novels.where(genre:"歴史")

  end

  def show_user_about_novel
    @novel = Novel.find(params[:id])
    @user = @novel.user
    @comments = @novel.comments.order(created_at: :ASC)
    if params[:comment_id].present?
      @comment = Comment.find(params[:comment_id])
    else
      @comment  = @novel.comments.build(user_id: current_user.id) if current_user
    end
  end

  def create_user_about
    @user_about = UserAbout.new(ssses_user_about_params)
    @user_about.user_id=current_user.id

    # うるまさんがいない時に実装
    shozoku = Shozoku.find_by(name:"超短編小説会")
    if shozoku.blank?
      shozoku = Shozoku.create!(name:"超短編小説会")
    end

    respond_to do |format|
      if @user_about.save

       puts "----------#{@user_about.id}--------------"
       puts "----------#{shozoku.id}--------------"

        # うるまさんがいない時に実装
        if session[:previous_url].include?("/ssses")
          UserAboutShozoku.create(user_about_id: @user_about.id, shozoku_id: shozoku.id)
        end

        format.html { redirect_to ssses_user_about_path+"?user_id=+#{@user_about.user_id}", notice: "名前登録成功です！" }
        format.json { render :index, status: :created, location: @user_about }
      else
        @user = User.find_by(id:params[:user_id])
        format.html { render :index, alert: '名前登録失敗です'}
        format.json { render json: @user_about.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_user_about
    @user_about = UserAbout.find_by(user_id:current_user.id)
    # logger.debug"-----------------#{@user_about}--------------------"

    respond_to do |format|

      # if @user_about.update_attributes(:locale => params[:user_about][:locale],:user_id => params[:user_id])
      puts "---------#{ssses_user_about_params.inspect}-------------"
      if @user_about.update(ssses_user_about_params)

        format.html { redirect_to ssses_user_about_path+"?user_id=#{@user_about.user_id}", notice: "変更完了です！" }
        format.json { render :index, status: :created, location: @user_about }
      else
        @user = User.find_by(id:params[:user_id])
        format.html { render :index, alert: '住所変更失敗です'}
        format.json { render json: @user_about.errors, status: :unprocessable_entity }
      end
    end
  end

  def comment_create
    @comment = Comment.new(comment_params)

    # うるまさんがいない時に実装　4/7
    if @comment.user_id == current_user.id

      respond_to do |format|
        @novel = Novel.find(params[:comment][:novel_id])
        if @comment.save
          if @novel.user_id == current_user.id
            CommentNotice.create(user_id:@comment.user_id,comment_id:@comment.id,novel_id:@comment.novel_id,read_flag:true)
          else
            CommentNotice.create(user_id:@comment.user_id,comment_id:@comment.id,novel_id:@comment.novel_id,read_flag:false)
          end
            CommentShougou.create_shougou(current_user)

          if params[:flag]
            format.html { redirect_to ssses_show_user_about_novel_path(@comment.novel_id), notice: 'Comment was successfully created.' }
          else
            format.html { redirect_to sss_path(@comment.novel_id), notice: 'Comment was successfully created.' }
          end
          format.json { render :show, status: :created, location: @comment }
        else
          @user = User.find(@novel.user_id)
          @user_about=UserAbout.find_by(user_id:@user.id)
          @comments = @novel.comments.all
          # @comment  = @novel.comments.build(user_id: current_user.id) if current_user

          format.html { render :show }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end

    end


  end



  def comment_update
    @comment = Comment.find(params[:comment_id])

    # うるまさんがいない時に実装
    if @comment.user_id = current_user.id


      respond_to do |format|
        if @comment.update(comment_params)
          if params[:flag]
            format.html { redirect_to ssses_show_user_about_novel_path(@comment.novel_id), notice: 'コメントを修正しました！' }
            format.json { render :show, status: :created, location: @comment }
          else
            format.html { redirect_to sss_path(@comment.novel_id), notice: 'コメントを修正しました！' }
            format.json { render :show, status: :created, location: @comment }
          end
        else
          format.html { render :show }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end

    end

  end

  def message_ichiran

    receiver_you_ids = Message.where(sender_id: current_user.id).pluck(:receiver_id).uniq
    sender_you_ids =  Message.where(receiver_id: current_user.id).pluck(:sender_id).uniq

    you_ids = (receiver_you_ids + sender_you_ids).uniq


    @messages = []
    you_ids.each do |you_id|
      if Message.exists?(sender_id: current_user.id, receiver_id: you_id)
        send_message = Message.where(sender_id: current_user.id, receiver_id: you_id).last
      end

      if Message.exists?(sender_id: you_id, receiver_id:  current_user.id)
        receiver_message = Message.where(sender_id: you_id, receiver_id: current_user.id).last
      end

      if send_message.present? && receiver_message.present?
        if send_message.created_at > receiver_message.created_at
        @messages << send_message
        else
        @messages << receiver_message
        end

      elsif send_message.present? && receiver_message.blank?
      @messages << send_message

      elsif send_message.blank? && receiver_message.present?
      @messages << receiver_message

      end

     end

  end

  private

  #  うるまさんいない時　9/17
  def get_message_notice
    if user_signed_in?
        @message_notice = Message.where(receiver_id:current_user.id,read_flag:false).count
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_novel
    @novel = Novel.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sss_params
    params.require(:novel).permit(:genre,:genre2_list,:title, :sakushamei,:honbun,:user_id,:past_work,:atogaki)
  end

  def ssses_user_about_params
    params.require(:user_about).permit(:name,:gender,:birthday,:locale,:user_id,:image)
  end

  def nanasi_user_about!
    if current_user.user_about.nil?
      redirect_to(user_user_abouts_path(:user_id => current_user.id))
    end
  end

  def tanin_ijiri!
    if @novel.user_id != current_user.id
      redirect_to(@novel)
    end
  end

  def comment_params
    params.require(:comment).permit(:user_id, :novel_id, :body)
  end

  def check_notice
    if params[:notice]
      notices = CommentNotice.where(novel_id: params[:id], read_flag: false).where.not(user_id: current_user.id)
      notices.each do |notice|
        notice.read_flag = true
        notice.save
        logger.debug "------------通過 ----------------"
      end
    end
  end

  def get_notices
    if user_signed_in?
      novel_ids = current_user.novels.pluck(:id)
      @comment_notices = []
      novel_ids.each do |novel_id|
        notice = CommentNotice.where(novel_id: novel_id, read_flag: false).where.not(user_id: current_user.id).first
        if notice.present?
          @comment_notices << notice
        end
      end
    end
  end
  
  def get_deadline
    @today = Time.zone.now
   # 12月になったら手動で年を変更する
    @happyou = Date.new(2023,2,25)
    @deadline = Date.new(2023,2,25)
  end


end
