# class ImagePreviewInput < SimpleForm::Inputs::FileInput
#   def input(wrapper_options = nil)
#     # :preview_version is a custom attribute from :input_html hash, so you can pick custom sizes
#     # input_htmlのオプションに:preview_versionがある場合、削除し値を保存
#     version = input_html_options.delete(:preview_version)
#     out = '' # the output string we're going to build
#     # check if there's an uploaded file (eg: edit mode or form not saved)
#     # attribute_nameが存在するか確認
#     # 今回の場合、imageが返ってくる
#     if object.send("#{attribute_name}?")
#       # append preview image to output
#       # プレビュー画像の表示
#
#       out << template.image_tag(object.send(attribute_name).tap {|o| break o.send(version) if version}.send('url'),:style=>"width:100%;height:auto;")
#
#     # else
#     # 存在しない場合、デフォルトの画像を表示
#     # out << template.image_tag('default.png', size: "80x80")
#     end
#     # allow multiple submissions without losing the tmp version
#     out << @builder.hidden_field("#{attribute_name}_cache").html_safe
#     # append file input. it will work accordingly with your simple_form wrappers
#     (out << @builder.file_field(attribute_name, input_html_options)).html_safe
#   end
#
# end
