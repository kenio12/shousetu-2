# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick


  # if Rails.env.development?
  #   storage :file
  # elsif Rails.env.test?
  #   storage :file
  # else
  #   storage :fog
  # end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # include Cloudinary::CarrierWave


  # version :post do
  #   process :resize_to_fill => [1170, 307]
  # end
  #
  # version :post_block do
  #   process :effect=>"art:incognito"
  #   process :resize_to_fill => [170, 170, :north]
  # end
  #
  # version :user_profile do
  #   process :resize_to_fill => [125, 125, :north]
  # end
  #
  # version :thumb do
  #   process :resize_to_limit => [600, 600]
  # end
  #
  # version :min_thumb do
  #   process :resize_to_limit => [50, 50]
  # end
  #
  # version :user_about_thumb do
  #   process :resize_to_fill => [100, 100]
  # end
  # version :user_about_min_thumb do
  #   process :resize_to_fill => [50, 50]
  # end

  def fix_exif_rotation
    manipulate! do |img|
      img.auto_orient
    end
  end
  process :fix_exif_rotation

end

# # encoding: utf-8
#
# # app/uploaders/image_uploader.rb
# class ImageUploader < CarrierWave::Uploader::Base
#   # include CarrierWave::RMagick
#   include Cloudinary::CarrierWave
#
#   storage :file
#
# # 保存形式をJPGにする
#   process :convert => 'jpg'
#
# # スマホ写真の向きを正しくする
#   process :fix_rotate
#
#   def fix_rotate
#
# #   # これでrmagick インスタンスにアクセスできるようになるらしい
# #   manipulate! do |img|
# #   # 向きを変えて、変数imgに入れる
# #     img = img.auto_orient!
# #     yield(img) if block_given?
# #     img
# #   end
# # end
#
#   # 保存するディレクトリ名
#   # def store_dir
#   #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
#   # end
#
#   # thumb バージョン(width 400px x height 200px)
#   version :thumb do
#     process :resize_to_fit => [600, 600]
#   end
#
#   # index用に小さくサムネイルするバージョン作成
#   version :min_thumb do
#     process :resize_to_fit => [50, 50]
#   end
#
#
#   # 許可する画像の拡張子
#   def extension_white_list
#     %w(jpg jpeg gif png)
#   end
#
#   # 変換したファイルのファイル名の規則
#   def filename
#     "#{Time.now.strftime('%Y%m%d%H%M%S')}.jpg" if original_filename.present?
#   end
#
#   def default_url
#   "/assets/fallback/" + [version_name, "default.png"].compact.join('_')
#   end
#
# end
