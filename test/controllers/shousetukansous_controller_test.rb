require 'test_helper'

class ShousetukansousControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shousetukansou = shousetukansous(:one)
  end

  test "should get index" do
    get shousetukansous_url
    assert_response :success
  end

  test "should get new" do
    get new_shousetukansou_url
    assert_response :success
  end

  test "should create shousetukansou" do
    assert_difference('Shousetukansou.count') do
      post shousetukansous_url, params: { shousetukansou: { chosha: @shousetukansou.chosha, comment: @shousetukansou.comment, hyouka: @shousetukansou.hyouka, image: @shousetukansou.image, shoseki: @shousetukansou.shoseki, shuppansha: @shousetukansou.shuppansha, user_id: @shousetukansou.user_id } }
    end

    assert_redirected_to shousetukansou_url(Shousetukansou.last)
  end

  test "should show shousetukansou" do
    get shousetukansou_url(@shousetukansou)
    assert_response :success
  end

  test "should get edit" do
    get edit_shousetukansou_url(@shousetukansou)
    assert_response :success
  end

  test "should update shousetukansou" do
    patch shousetukansou_url(@shousetukansou), params: { shousetukansou: { chosha: @shousetukansou.chosha, comment: @shousetukansou.comment, hyouka: @shousetukansou.hyouka, image: @shousetukansou.image, shoseki: @shousetukansou.shoseki, shuppansha: @shousetukansou.shuppansha, user_id: @shousetukansou.user_id } }
    assert_redirected_to shousetukansou_url(@shousetukansou)
  end

  test "should destroy shousetukansou" do
    assert_difference('Shousetukansou.count', -1) do
      delete shousetukansou_url(@shousetukansou)
    end

    assert_redirected_to shousetukansous_url
  end
end
