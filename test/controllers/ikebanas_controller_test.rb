require 'test_helper'

class IkebanasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ikebana = ikebanas(:one)
  end

  test "should get index" do
    get ikebanas_url
    assert_response :success
  end

  test "should get new" do
    get new_ikebana_url
    assert_response :success
  end

  test "should create ikebana" do
    assert_difference('Ikebana.count') do
      post ikebanas_url, params: { ikebana: {  } }
    end

    assert_redirected_to ikebana_url(Ikebana.last)
  end

  test "should show ikebana" do
    get ikebana_url(@ikebana)
    assert_response :success
  end

  test "should get edit" do
    get edit_ikebana_url(@ikebana)
    assert_response :success
  end

  test "should update ikebana" do
    patch ikebana_url(@ikebana), params: { ikebana: {  } }
    assert_redirected_to ikebana_url(@ikebana)
  end

  test "should destroy ikebana" do
    assert_difference('Ikebana.count', -1) do
      delete ikebana_url(@ikebana)
    end

    assert_redirected_to ikebanas_url
  end
end
