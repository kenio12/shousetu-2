class ChangeDatatypeHonbunOfNovel < ActiveRecord::Migration[5.0]
  def change
    change_column :novels, :honbun, :text
  end
end
