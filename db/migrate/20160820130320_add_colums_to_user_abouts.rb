class AddColumsToUserAbouts < ActiveRecord::Migration[5.0]
  def change
    add_column :user_abouts, :gender, :string
    add_column :user_abouts, :locale, :string
    add_column :user_abouts, :birthday, :string
    add_column :user_abouts, :image, :string
  end
end
