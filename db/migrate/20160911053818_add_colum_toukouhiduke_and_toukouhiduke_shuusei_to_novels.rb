class AddColumToukouhidukeAndToukouhidukeShuuseiToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :toukouhiduke, :datetime
    add_column :novels, :toukouhiduke_shuusei, :datetime
  end
end
