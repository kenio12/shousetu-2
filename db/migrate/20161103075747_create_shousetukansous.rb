class CreateShousetukansous < ActiveRecord::Migration[5.0]
  def change
    create_table :shousetukansous do |t|
      t.text :image
      t.string :shuppansha
      t.string :chosha
      t.string :shoseki
      t.text :comment
      t.string :hyouka
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
