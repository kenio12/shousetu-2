class AddColumnGenreToIkebanas < ActiveRecord::Migration[5.0]
  def change
    add_column :ikebanas, :genre, :string
  end
end
