class ChangeColumn2PastWorkToNovels < ActiveRecord::Migration[5.0]
  def change
    change_column :novels, :past_work, :boolean, default: false
  end
end
