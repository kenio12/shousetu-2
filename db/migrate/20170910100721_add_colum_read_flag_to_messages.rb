class AddColumReadFlagToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :read_flag, :boolean,default:false,null: false
  end
end
