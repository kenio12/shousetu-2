class ChangeColumnPastWorkToNovels < ActiveRecord::Migration[5.0]
  def change
    change_column :novels, :past_work, :boolean, null: false
  end
end
