class ChangeDatatypeBirthdayToUserAbouts < ActiveRecord::Migration[5.0]
  def change
    if Rails.env != "development"
    change_column :user_abouts, :birthday, 'date USING CAST(birthday AS date)'
    end
   end
end
