class CreateIkebanaFlowers < ActiveRecord::Migration[5.0]
  def change
    create_table :ikebana_flowers do |t|
      t.references  :ikebana,  index: true, foreign_key: true
      t.references  :flower, index: true, foreign_key: true
      t.timestamps
    end
  end
end
