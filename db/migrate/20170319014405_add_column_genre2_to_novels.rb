class AddColumnGenre2ToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :genre2, :string
  end
end
