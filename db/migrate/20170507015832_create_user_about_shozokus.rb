class CreateUserAboutShozokus < ActiveRecord::Migration[5.0]
  def change
    create_table :user_about_shozokus do |t|
      t.integer :user_about_id
      t.integer :shozoku_id

      t.timestamps
    end
  end
end
