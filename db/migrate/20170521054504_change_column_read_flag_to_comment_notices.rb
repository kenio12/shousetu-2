class ChangeColumnReadFlagToCommentNotices < ActiveRecord::Migration[5.0]
  def change
    change_column :comment_notices, :read_flag, :boolean, default:false,null: false
  end
end
