class AddColumnExpiredAtToNews < ActiveRecord::Migration[5.0]
  def change
    add_column :news, :expired_at, :datetime
  end
end
