class CreateMaturiateShougous < ActiveRecord::Migration[5.0]
  def change
    create_table :maturiate_shougous do |t|
      t.integer :user_id
      t.string :shougou

      t.timestamps
    end
  end
end
