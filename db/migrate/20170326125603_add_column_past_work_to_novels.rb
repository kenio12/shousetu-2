class AddColumnPastWorkToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :past_work, :boolean
  end
end
