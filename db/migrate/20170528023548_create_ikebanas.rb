class CreateIkebanas < ActiveRecord::Migration[5.0]
  def change
    create_table :ikebanas do |t|
      t.integer :user_id
      t.string :title
      t.string :image      
      t.text :message
      t.timestamps
    end
  end
end
