class CreateNovels < ActiveRecord::Migration[5.0]
  def change
    create_table :novels do |t|
      t.string :title
      t.string :sakushamei
      t.string :honbun
      t.timestamps
    end
  end
end
