class AddColumnFlagAndGenreToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :flag, :boolean, default:false, null: false
    add_column :novels, :genre, :integer, default:0, null: false
  end
end
