class AddUserIdToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :user_id, :integer
    add_index :novels, :user_id
  end
end
