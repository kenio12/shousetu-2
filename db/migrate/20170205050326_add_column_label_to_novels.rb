class AddColumnLabelToNovels < ActiveRecord::Migration[5.0]
  def change
    add_column :novels, :label, :string
  end
end
