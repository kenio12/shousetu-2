class CreateCommentNotices < ActiveRecord::Migration[5.0]
  def change
    create_table :comment_notices do |t|
      t.integer :user_id
      t.integer :comment_id
      t.integer :novel_id
      t.boolean :read_flag

      t.timestamps
    end
  end
end
