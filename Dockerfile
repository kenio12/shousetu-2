# syntax=docker/dockerfile:1
FROM ruby:3.1.2
RUN apt-get update -qq \
  && apt-get install -y nodejs postgresql-client npm graphviz vim \
  && rm -rf /var/lib/apt/lists/* \
  && npm install --global yarn

WORKDIR /tmp
COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs=4

WORKDIR /app
COPY . /app

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
